# Hive Ticker Gnome Extension

Hive Ticker is a simple GNOME shell extension that displays HIVE price on the system status area. Price is gathered from CoinGecko API. The ticker is updated every minute and shows a panel with additional pieces of information when clicked.

![](./screenshots/ticker.png)

### Preferences
 - **Use colors**: for default, Hive Ticker will use red and green indicators for price changes. If you find it distracting, you can set it to `false`.

![](./screenshots/preferences.png)

## Installation

### Official GNOME Shell Extensions website

[![get-it-on-ego](https://user-images.githubusercontent.com/4411084/79335031-a4938100-7f21-11ea-981c-a629684967b1.png)](https://extensions.gnome.org/extension/2941/hive-ticker/)

### Manually from git repository

Clone this repository and prepare extension directory:
```shell script
git clone https://gitlab.com/engrave/hive-ticker-gnome-extension
mkdir -p ~/.local/share/gnome-shell/extensions/hive-ticker@engrave.dev
cp -R hive-ticker-gnome-extension/src/* ~/.local/share/gnome-shell/extensions/hive-ticker@engrave.dev/
```

Restart GNOME shell:
 - Press `Alt + F2`, type `r` and press `Enter`.

Enable extension:
 - `gnome-shell-extension-tool --enable hive-ticker@engrave.dev`

## Development

Contributions are welcome but make sure you follow [requirements and tips for getting your GNOME Shell Extension approved](https://wiki.gnome.org/Projects/GnomeShell/Extensions/Review).

### ESLint

Gitlab CI/CD is configured to make eslint job on every merge request. Changes cannot be merged if the pipeline fails. 

Install and run ESLint before you do any contribution:

```shell script
npm i eslint
node_modules/eslint/bin/eslint.js ./src --fix
```

## Supported GNOME versions:

Extension was developed and tested for GNOME `3.28`. If you can test different versions, please let me know if it works and open issue if not.
