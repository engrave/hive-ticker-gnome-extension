/* eslint-disable no-invalid-this */
'use strict';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

function init() {
}

function buildPrefsWidget() {

    // Copy the same GSettings code from `extension.js`
    let gschema = Gio.SettingsSchemaSource.new_from_directory(
        Me.dir.get_child('schemas').get_path(),
        Gio.SettingsSchemaSource.get_default(),
        false
    );

    this.settings = new Gio.Settings({
        settings_schema: gschema.lookup('dev.engrave.hive.ticker', true)
    });

    // Create a parent widget that we'll return from this function
    let prefsWidget = new Gtk.Grid({
        margin: 18,
        column_spacing: 12,
        row_spacing: 12,
        visible: true
    });

    let title = new Gtk.Label({
        // eslint-disable-next-line prefer-template
        label: '<b>' + Me.metadata.name + ' Extension Preferences</b>',
        halign: Gtk.Align.START,
        use_markup: true,
        visible: true
    });
    prefsWidget.attach(title, 0, 0, 2, 1);

    // Create a label & switch for `use-colors`
    let colorsToggleLabel = new Gtk.Label({
        label: 'Use colors to indicate price change:',
        halign: Gtk.Align.START,
        visible: true
    });

    prefsWidget.attach(colorsToggleLabel, 0, 2, 1, 1);

    let colorsToggle = new Gtk.Switch({
        active: this.settings.get_boolean('use-colors'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(colorsToggle, 1, 2, 1, 1);

    // Create a label & switch for `show-btc`
    let showBtcToggleLabel = new Gtk.Label({
        label: 'Show prices and changes in BTC:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(showBtcToggleLabel, 0, 3, 1, 1);

    let showBtcToggle = new Gtk.Switch({
        active: this.settings.get_boolean('show-btc'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(showBtcToggle, 1, 3, 1, 1);


    // Bind the switch to the `use-colors` key
    this.settings.bind('use-colors', colorsToggle, 'active', Gio.SettingsBindFlags.DEFAULT);
    this.settings.bind('show-btc', showBtcToggle, 'active', Gio.SettingsBindFlags.DEFAULT);

    // Return our widget which will be added to the window
    return prefsWidget;
}
