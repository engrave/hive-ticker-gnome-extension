'use strict';

const Soup = imports.gi.Soup;

// eslint-disable-next-line no-unused-vars
var CoinGecko = class CoinGecko {
    constructor() {
        this.url = 'https://api.coingecko.com/api/v3/coins/hive';
    }

    getTicker() {
        return new Promise((resolve, reject) => {
            this.session = new Soup.Session();
            const message = Soup.form_request_new_from_hash('GET', this.url, {});
            this.session.queue_message(message, (session, result) => {
                if (result.status_code !== 200) {
                    return reject(result.status_code);
                }

                const ticker = JSON.parse(result.response_body.data);
                return resolve(ticker);
            });
        });
    }

    abort() {
        if (this.session) {
            this.session.abort();
        }

        this.session = undefined;
    }
};


