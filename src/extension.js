'use strict';

const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const {HiveIndicator} = Me.imports.ui.HiveIndicator;

let indicator;

function init() {
}

function enable() {
    indicator = new HiveIndicator();
    Main.panel.addToStatusArea('hive-indicator', indicator.panel);
}

function disable() {
    indicator.abort();
    indicator.panel.destroy();
}
