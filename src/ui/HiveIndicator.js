'use strict';

const GObject = imports.gi.GObject;
const St = imports.gi.St;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const PanelMenu = imports.ui.panelMenu;
const Mainloop = imports.mainloop;

const Adapter = Me.imports.modules.coingecko;
const {HiveIcon} = Me.imports.ui.components.HiveIcon;
const {PriceChangeBox} = Me.imports.ui.components.PriceChangeBox;
const {CurrentPriceBox} = Me.imports.ui.components.CurrentPriceBox;
const {TickerPanelItem} = Me.imports.ui.components.TickerPanelItem;

// eslint-disable-next-line no-unused-vars
var HiveIndicator = GObject.registerClass({
    GTypeName: 'HiveIndicator'
}, class HiveIndicator extends GObject.Object {
    _init() {
        this.panel = new PanelMenu.Button(0, 'Hive Indicator', false);

        this.interval_s = 60;

        this.price = new CurrentPriceBox();
        this.change = new PriceChangeBox();

        const box = new St.BoxLayout();

        box.add(new HiveIcon());
        box.add(this.price);
        box.add(this.change);

        this.panel.actor.add_child(box);

        this.adapter = new Adapter.CoinGecko();
        this._buildPanel();
        this._refresh();
    }

    async _refresh() {
        await this._updateTicker();
        this._removeTimeout();
        this.timeout = Mainloop.timeout_add_seconds(this.interval_s, () => this._refresh());
        return true;
    }

    async _updateTicker() {
        try {
            const ticker = await this.adapter.getTicker();
            this._buildPanel(ticker);
            this.price.setTicker(ticker);
            this.change.setTicker(ticker);
        } catch (e) {
            this._setError('Network error');
            log(e);
        }
    }

    _buildPanel(ticker) {
        this.panel.menu.removeAll();
        if (!ticker) {
            this.panel.menu.addMenuItem(new TickerPanelItem('Ticker not available'));
        } else {
            const currentPrice = `Current price: ${ticker.market_data.current_price.usd} USD`;
            const currentPriceBtc = `Current price: ${ticker.market_data.current_price.btc} BTC`;

            const allTimeHigh = `All time high: ${ticker.market_data.ath.usd} USD`;
            const allTimeHighBtc = `All time high: ${ticker.market_data.ath.btc} BTC`;

            const allTimeLow = `All time low: ${ticker.market_data.atl.usd} USD`;
            const allTimeLowBtc = `All time low: ${ticker.market_data.atl.btc} BTC`;

            const priceChangePercentage24h = `Price change 24h: ${ticker.market_data.price_change_percentage_24h_in_currency.usd}%`;
            const priceChangePercentage24hBtc = `Price change 24h: ${ticker.market_data.price_change_percentage_24h_in_currency.btc}%`;

            const priceChangePercentage7d = `Price change 7d: ${ticker.market_data.price_change_percentage_7d_in_currency.usd}%`;
            const priceChangePercentage7dBtc = `Price change 7d: ${ticker.market_data.price_change_percentage_7d_in_currency.btc}%`;

            this.panel.menu.addMenuItem(new TickerPanelItem('USD:'));
            this.panel.menu.addMenuItem(new TickerPanelItem(currentPrice));
            this.panel.menu.addMenuItem(new TickerPanelItem(allTimeHigh));
            this.panel.menu.addMenuItem(new TickerPanelItem(allTimeLow));
            this.panel.menu.addMenuItem(new TickerPanelItem(priceChangePercentage24h));
            this.panel.menu.addMenuItem(new TickerPanelItem(priceChangePercentage7d));
            this.panel.menu.addMenuItem(new TickerPanelItem(''));
            this.panel.menu.addMenuItem(new TickerPanelItem('BTC:'));
            this.panel.menu.addMenuItem(new TickerPanelItem(currentPriceBtc));
            this.panel.menu.addMenuItem(new TickerPanelItem(allTimeHighBtc));
            this.panel.menu.addMenuItem(new TickerPanelItem(allTimeLowBtc));
            this.panel.menu.addMenuItem(new TickerPanelItem(priceChangePercentage24hBtc));
            this.panel.menu.addMenuItem(new TickerPanelItem(priceChangePercentage7dBtc));
        }
    }

    _setError(error) {
        this.price.setError(error);
        this.change.setTicker(null);
    }

    _removeTimeout() {
        if (this.timeout) {
            Mainloop.source_remove(this.timeout);
            this.timeout = null;
        }
    }

    abort() {
        this._removeTimeout();
        this.adapter.abort();
        this.change.destroy();
        this.price.destroy();
    }
});
