'use strict';

const GObject = imports.gi.GObject;
const St = imports.gi.St;
const Gio = imports.gi.Gio;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Clutter = imports.gi.Clutter;

// eslint-disable-next-line no-unused-vars
var PriceChangeBox = GObject.registerClass({
    GTypeName: 'PriceChangeBox'
}, class PriceChangeBox extends St.Label {
    _init() {

        const gschema = Gio.SettingsSchemaSource.new_from_directory(
            Me.dir.get_child('schemas').get_path(),
            Gio.SettingsSchemaSource.get_default(),
            false
        );

        this.settings = new Gio.Settings({settings_schema: gschema.lookup('dev.engrave.hive.ticker', true)});
        this.useColors = this.settings.get_value('use-colors').deep_unpack();
        this.showBtc = this.settings.get_value('show-btc').deep_unpack();

        this.change = null;
        this.changeBtc = null;
        this.ticker = null;

        this.onUserColorSettingsChange = this.settings.connect(
            'changed::use-colors',
            this._onUserColorsSettingChanged.bind(this)
        );

        this.onShowBtcSettingsChange = this.settings.connect(
            'changed::show-btc',
            this._onShowBtcSettingsChanged.bind(this)
        );

        super._init({text: '', y_align: Clutter.ActorAlign.CENTER});
    }

    _onUserColorsSettingChanged() {
        this.useColors = this.settings.get_value('use-colors').deep_unpack();
        this._setColor();
    }

    _onShowBtcSettingsChanged() {
        this.showBtc = this.settings.get_value('show-btc').deep_unpack();
        this._updateTicker();
    }

    _setColor() {
        if (this.useColors) {
            const change = this.showBtc ? this.changeBtc : this.change;
            this.style_class = change < 0 ? 'indicator__change-negative' : 'indicator__change-positive';
        } else {
            this.style_class = '';
        }
    }

    setTicker(ticker) {
        this.ticker = ticker;
        this._updateTicker();
    }

    _updateTicker() {
        if (this.ticker) {
            this.change = this.ticker.market_data.price_change_percentage_24h_in_currency.usd;
            this.changeBtc = this.ticker.market_data.price_change_percentage_24h_in_currency.btc;

            const change = this.showBtc ? this.changeBtc : this.change;

            this.set_text(` (${change.toFixed(1)}%)`);
            this._setColor();
        } else {
            this.set_text('');
        }
    }

    destroy() {
        this.settings.disconnect(this.onUserColorSettingsChange);
        this.settings.disconnect(this.onShowBtcSettingsChange);
        super.destroy();
    }

});
