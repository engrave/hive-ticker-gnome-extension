'use strict';

const St = imports.gi.St;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Gio = imports.gi.Gio;

// eslint-disable-next-line no-unused-vars
var HiveIcon = class HiveIcon extends St.Icon {
    constructor() {
        const gicon = Gio.icon_new_for_string(`${Me.path}/assets/hive.png`);
        super({gicon, style_class: 'system-status-icon'});
    }
};
