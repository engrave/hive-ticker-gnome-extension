'use strict';

const GObject = imports.gi.GObject;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

// eslint-disable-next-line no-unused-vars
var CurrentPriceBox = GObject.registerClass({
    GTypeName: 'CurrentPriceBox'
}, class CurrentPriceBox extends St.Label {
    _init() {

        const gschema = Gio.SettingsSchemaSource.new_from_directory(
            Me.dir.get_child('schemas').get_path(),
            Gio.SettingsSchemaSource.get_default(),
            false
        );

        this.settings = new Gio.Settings({settings_schema: gschema.lookup('dev.engrave.hive.ticker', true)});

        this.showBtc = this.settings.get_value('show-btc').deep_unpack();

        this.ticker = null;

        this.onShowBtcSettingsChange = this.settings.connect(
            'changed::show-btc',
            this._onShowBtcSettingsChanged.bind(this)
        );

        super._init({text: 'Loading...', y_align: Clutter.ActorAlign.CENTER});
    }

    setTicker(ticker) {
        this.ticker = ticker;
        this._updateTicker();
    }

    setError(error) {
        this.set_text(error);
    }

    _updateTicker() {
        if (this.ticker) {
            if (this.showBtc) {
                this.set_text(`BTC ${this.ticker.market_data.current_price.btc}`);
            } else {
                this.set_text(`$${this.ticker.market_data.current_price.usd.toFixed(3)}`);
            }
        }
    }

    _onShowBtcSettingsChanged() {
        this.showBtc = this.settings.get_value('show-btc').deep_unpack();
        this._updateTicker();
    }

    destroy() {
        this.settings.disconnect(this.onShowBtcSettingsChange);
        super.destroy();
    }
});
