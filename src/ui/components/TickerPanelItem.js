const PopupMenu = imports.ui.popupMenu;

// eslint-disable-next-line no-unused-vars
var TickerPanelItem = class TickerPanelItem extends PopupMenu.PopupMenuItem {
    constructor(text) {
        super(text, {reactive: false});
    }
};
